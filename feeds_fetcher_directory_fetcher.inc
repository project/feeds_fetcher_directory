<?php

/**
 * @file
 *
 * Provides the Feeds Directory Fetcher to feeds.
 */

/**
 * Fetches a file from a specified directory.
 *
 * This fetcher will keep track of the files that it has already fetched, and
 * will not re-fetch them, unless specifically told to.
 */
class feeds_fetcher_directory_fetcher extends FeedsFileFetcher {

 /**
   * Fetch content from a source and return it.
   *
   * Every class that extends FeedsFetcher must implement this method.
   *
   * @param $source
   *   Source value as entered by user through sourceForm().
   */
  public function fetch(FeedsSource $source) {
    $source_config = $source->getConfigFor($this);
    $dir = file_create_path($source_config['source']);
    $files = file_scan_directory($dir, ".*");
    foreach ($files as $file) {
      $filepath = $file->filename;
      if (isset($source_config['feed_files_fetched'][$filepath])) {
        continue;
      }
      $source_config['feed_files_fetched'][$filepath] = TRUE;
      $blah = $source->getConfig();
      $blah[get_class($this)] = $source_config;
      $source->setConfig($blah);
      return new FeedsFileBatch($filepath);
    }
    // Return an empty FeedsImportBatch if we didn't get anything from the directory:
    return new FeedsImportBatch();
  }

 /**
   * Source form.
   */
  public function sourceForm($source_config) {
    $form = array();
    // When renaming, do not forget feeds_vews_handler_field_source class.
    $form['source'] = array(
      '#type' => 'textfield',
      '#title' => t('Directory'),
      '#description' => t('Specify a directory in the filesystem to scan for feed data.'),
      '#default_value' => isset($source_config['source']) ? $source_config['source'] : '',
    );
    $form['reset'] = array(
      '#type' => 'checkbox',
      '#title' => t('Re-fetch entire directory'),
      '#description' => t('When checked will re-fetch previously imported data.'),
      '#default_value' => 0,
    );
    $form['feed_files_fetched'] = array(
      '#type' => 'value',
      '#value' => isset($source_config['feed_files_fetched']) ? $source_config['feed_files_fetched'] : array(),
    );
    return $form;
  }

 /**
   * Override parent::sourceFormValidate().
   */
  public function sourceFormValidate(&$values) {
    // If $values['source'] is not empty, make
    // sure that this directory is within Drupal's files directory as otherwise
    // potentially any file that the web server has access could be exposed.
    $path = $values['source'];
    if (!file_create_path($path)) {
      form_set_error('feeds][source', t('Directory needs to point to a directory in your Drupal file system path.'));
    }
    if ($values['reset']) {
      $values['feed_files_fetched'] = array();
      $values['reset'] = 0;
    }
  }

 /**
   * Override parent::configDefaults().
   */
  public function configDefaults() {
    return array(
      'feed_files_fetched' => array(),
    );
  }
}
